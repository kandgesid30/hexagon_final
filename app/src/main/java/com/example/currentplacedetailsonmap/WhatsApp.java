package com.example.currentplacedetailsonmap;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class WhatsApp extends AppCompatActivity {
    public void openWhatsApp(View view, String toNumber, String latitude, String longitude){
        try {
//            String latitude = "23.0225";
//            String longitude = "72.5714";
            String text = "http://maps.google.com/maps?saddr=" + latitude + "," + longitude;

//            String toNumber = "919403256432"; // Replace with mobile phone number without +Sign or leading zeros, but with country code
            //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.


            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone="+toNumber +"&text="+text));
//            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone="+"919763847172" +"&text="+text));
//            intent.setData(Uri.parse("http://api.whatsapp.com/send?phone="+"919370026505" +"&text="+text));
            startActivity(intent);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}

